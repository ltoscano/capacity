<?php

/**
 * Menu code for www.kde.org, this is the www/media version
 * Written by Christoph Cullmann <cullmann@kde.org>
 * Altered by Olaf Schmidt <ojschmidt@kde.org>
 * Based on the cool menu code from usability.kde.org written by Simon Edwards <simon@simonzone.com>
 */
 
/**
 * Added PlasmaMenu code
 * Written by Sayak Banerjee <sayakb@kde.org>
 */

class BaseMenu
{
  var $menu_root;
  var $menu_baseurl;
  var $items = array();
  var $current_relativeurl;
  var $name;

  function BaseMenu ($menu_root, $menu_baseurl, $current_relativeurl)
  {
    global $name;

    if (! (substr ($menu_baseurl, strlen($menu_baseurl)-1, 1) == "/"))
      $menu_root = $menu_root."/";

    if (! (substr ($menu_baseurl, strlen($menu_baseurl)-1, 1) == "/"))
      $menu_baseurl = $menu_baseurl."/";

    if (! (substr ($menu_root, strlen($menu_root)-1, 1) == "/"))
      $menu_root = $menu_root."/";

    $current_relativeurl = str_replace ("index.php", "", $current_relativeurl);
    $current_relativeurl = str_replace ("//", "/", $current_relativeurl);

    $this->menu_root = $menu_root;
    $this->menu_baseurl = $menu_baseurl;
    $this->current_relativeurl = $current_relativeurl;
    $this->name = "";

    if (file_exists ($this->menu_root."menu.inc") && is_readable ($this->menu_root."menu.inc"))
      include ($this->menu_root."menu.inc");
  }

  function setName ($name)
  {
    $this->name = $name;
  }

  function &appendSection ($name,$link='',$hidden = false)
  {
    $section = new MenuSection ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl, $name, '', $link, $hidden);
    $this->items[] =& $section;

    return $section;
  }

  function show ()
  {
    print "<!-- ".htmlentities($this->menu_root)."-->\n";
    print "<!-- ".htmlentities($this->current_relativeurl)."-->\n";
    print "<!-- ".htmlentities($this->menu_baseurl)."-->\n";

    if(count($this->items))
    {
      for($i=0; $i < count($this->items); $i++)
      {
       $this->items[$i]->show ();
      }
    }
  }

  /**
   * Return the current active section of the menu
   * default to the first section in the menu if no other marked active
   * this is the case for the index.php for example !
   */
  function activeSection ()
  {
    for($i=0; $i < count($this->items); $i++)
    {
      if ($this->items[$i]->active)
        return $i;
    }

    return 0;
  }

  function activeSectionName ()
  {
    return $this->items[$this->activeSection()]->name;
  }

  function showLocation ()
  {
    if ($this->name)
      print '<a href="'.($this->menu_baseurl).'" accesskey="1">'.$this->name.'</a>';

    if(count($this->items))
    {
      for($i=0; $i < count($this->items); $i++)
      {
       $this->items[$i]->showLocation ();
      }
    }
  }
}

class MenuSection
{
  var $menu_root;
  var $menu_baseurl;
  var $name;
  var $id;
  var $items = array();
  var $current_relativeurl;
  var $link;
  var $active;
  var $hidden;

  function MenuSection ($menu_root, $menu_baseurl, $current_relativeurl, $name, $id = "", $link, $hidden)
  {
    if (substr ($link, -1) != "/")
      $link = $link."/";
    $this->menu_root = $menu_root;
    $this->link = $link;
    $this->menu_baseurl = $menu_baseurl;
    $this->current_relativeurl = $current_relativeurl;
    $this->name = $name;
    if ($id == "")
      $this->id = strtolower(preg_replace("([^0-9a-zA-Z])", "", $name));
    else
      $this->id = $id;

    if( $link != "/" ) {
      $this->active = strcmp ($link, "/".$current_relativeurl) == 0;
    } else {
      $this->active = false;
    }

    $this->hidden = $hidden;
  }

  function appendLink ($name, $link, $relative_link = true, $is_dir = false, $icon = false, $new = false)
  {
    $item = new Menu ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl,
                      $name, $link, $relative_link, false, $icon, $new);
    array_push($this->items,$item);

    return $item;
  }

  function appendDir ($name, $dir, $icon = false, $new = false)
  {
    $item = new Menu ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl,
                      $name, $dir, true, true, $icon, $new);
    array_push($this->items,$item);

    return $item;
  }

  function show ()
  { global $template_menulist1, $template_menulist2, $template_menulist3, $templatepath;

    if( $this->hidden ) {
        return;
    }

    print $template_menulist1."\n";
    if ($templatepath == "chihuahua/")
	print '<h2 id="cp-menu-'.$this->id.'"><a href="'.$this->link.'">'.$this->name."</a></h2>\n";
    else
	print '<h2 id="cp-menu-'.$this->id.'">'.$this->name."</h2>\n";
    print '<a href="#cp-skip-'.$this->id.'" class="cp-doNotDisplay">'.str_replace ('%1', $this->name, i18n_var ('Skip menu "%1"'))."</a>\n";
    print $template_menulist2."\n";

    print "<ul>\n";

    for($i=0; $i < count($this->items); $i++) {
      if ($this->items[$i] InstanceOf MenuSection) {
          continue;
      }
      $this->items[$i]->show ();
    }

    print "</ul>\n";
    print $template_menulist3."\n";
  }

  function showLocation ()
  {
    if ($this->active) {
        print ' &bull; '.$this->name;
        return;
    }
    for($i=0; $i < count($this->items); $i++)
    {
      if ($this->items[$i]->active || (isset($this->items[$i]->item_active) && $this->items[$i]->item_active)) {
        print ' &bull; <a href="'.$this->link.'">'.$this->name.'</a>';
        $this->items[$i]->showLocation ();
        break;
      }
    }
  }

  function &appendSection ($name,$link='')
  {
    $section = new MenuSection ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl, $name, '', $link);
    $this->items[] =& $section;

    return $section;
  }
}

class Menu {
  var $menu_root;
  var $menu_baseurl;
  var $items;
  var $name;
  var $link;
  var $relative_link;
  var $current_relativeurl;
  var $active;
  var $item_active = false;
  var $hidden;

  function Menu($menu_root, $menu_baseurl, $current_relativeurl, $name, $link, $relative_link, $is_dir, $icon=NULL, $new=NULL)
  {
    if ($is_dir && (substr ($link, -1) != "/"))
      $link = $link."/";
    $link = str_replace ("index.php", "", $link);

    $this->name = $name;
    $this->link = $link;
    $this->relative_link = $relative_link;
    $this->items = array();
    $this->active = strcmp ($link, $current_relativeurl) == 0;
    $this->menu_root = $menu_root;
    $this->menu_baseurl = $menu_baseurl;
    $this->current_relativeurl = $current_relativeurl;
    $this->icon = $icon;
    $this->new = $new;

    if ($is_dir)
    {
      $this->menu_root .= $link;
      $this->menu_baseurl .= $link;
      $this->link = "";

      if (strncmp ($link, $current_relativeurl, strlen ($link)) == 0)
      {
        $this->current_relativeurl = substr ($current_relativeurl, strlen ($link));
        $this->item_active = true;

        if (file_exists ($this->menu_root."menu.inc") && is_readable ($this->menu_root."menu.inc"))
          include ($this->menu_root."menu.inc");
      }
      else
        $this->current_relativeurl = "";
    }
  }

  function itemActive()
  {
    return $this->active || $this->item_active;
  }

  function setHidden()
  {
    $this->hidden = true;
  }

  function appendLink ($name, $link, $relative_link = true, $icon = false, $new = false)
  {
    $item = new Menu ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl,
                      $name, $link, $relative_link, false, false);
    array_push($this->items,$item);

    if ($item->itemActive())
      $this->item_active = true;

    return $item;
  }

  function appendDir ($name, $dir, $icon = false, $new = false)
  {
    $item = new Menu ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl,
                      $name, $dir, true, true, $icon, $new);
    array_push($this->items,$item);

    if ($item->itemActive())
      $this->item_active = true;

    return $item;
  }

  function show ()
  { global $template_menuitem1, $template_menuitem2;
    global $template_menusublist1, $template_menusublist2, $template_menusublist3;

    if( $this->hidden )
        return;

    if ($this->active)
        print "<li class=\"here\">";
    else
        print "<li>";

    if ($this->itemActive() && (count($this->items) > 0))
      print $template_menusublist1."\n";
    else
      print $template_menuitem1."\n";

      if (! empty($this->icon))
          print '<img src="'.$this->icon.'" alt="" width="16" height="16" />&nbsp;';
    if (! empty ($this->link) || ! empty ($this->menu_baseurl)) {
        print '<a href="';
        if ($this->relative_link)
            print $this->menu_baseurl;
        print $this->link.'">'.$this->name."</a>";
    }
    else
        print $this->name;

    if ($this->new)
    	print '<img src="/media/images/new.png" alt="new" />';

    if ($this->itemActive() && (count($this->items) > 0))
    {
      print $template_menusublist2."\n";

      $menu_started = false;

      for($i=0; $i < count($this->items); $i++) {
        if( $this->items[$i]->hidden ) {
            continue;
        }
        if( !$menu_started ) {
            print "<ul>\n";
            $menu_started = true;
        }
        $this->items[$i]->show ();
      }

      if( $menu_started ) {
          print "</ul>\n";
      }
      print $template_menusublist3."\n";
    }
    else
      print $template_menuitem2."\n";

    print "</li>\n";
  }

  function showLocation ()
  {
    if ($this->active || $this->item_active)
    {
      if ($this->menu_baseurl.$this->link != "/")
        if ($this->active)
          print ' &bull; '.$this->name;
        else
          print ' &bull; <a href="'.($this->menu_baseurl.$this->link).'">'.$this->name.'</a>';

      if (count($this->items) > 0)
      {
        for($i=0; $i < count($this->items); $i++)
        {
          $this->items[$i]->showLocation ();
        }
      }
    }
  }
}

/**
* The plasma menu builder class
* @author - Sayak Banerjee <sayakb@kde.org>
*/
class plasmaMenuBuilder
{			
	// Define the instance variables
	var $menus = array();
	var $menuEntries = array();
	var $shortNames = array();
	var $lastMenu = null;
	var $lastEntry = null;
	var $imagePath = "/media/images/plasmaMenu/";
	var $iconPath = "/media/images/plasmaMenu/";
	var $maxMenus = 5;
	var $checkMenuOverflow = true;
	var $breadcrumbSep = "&nbsp;&bull;&nbsp;";
	
	//
	// The constructor
	//
	function __construct()
	{
		$this->menus['name'] = array();
		$this->menus['icon'] = array();
		$this->menus['shadow'] = array();
	}
	
	//
	// Function to add a new root menu item
	//
	function addMenu($name, $link, $icon = false, $shadow = false)
	{
		if (count($this->menus['name']) == $this->maxMenus && $this->checkMenuOverflow)
		{
			// Show an error and exit
			die("<b>PlasmaMenu Error</b>: You cannot at more than {$this->maxMenus} menu" . ($this->maxMenus > 1 ? "s. " : ". ") .
						  "To disable this error, change the maxMenus value, or set checkMenuOverflow to false.");
		}
		else if (!in_array($name, $this->menus['name']))
		{
			// Add the menus
			$this->menus['name'][] = $name;
			$this->menus['link'][] = $link;
			$this->menus['icon'][] = $icon;
			$this->menus['shadow'][] = $shadow;
			
			// Set the last menu
			$this->lastMenu = $name;
		}
		else
		{
			// Menu already exists
			die("<b>PlasmaMenu Error</b>: Duplicate menu '{$name}'.");
		}
	}
	
	//
	// Function to add a root menu entry
	//
	function addMenuEntry($name, $link, $icon = false)
	{
		// Get the parent
		$parent = $this->lastMenu;
	
		// Check if the root menu exists
		if (!is_null($parent) && $this->menuExists($parent))
		{			
			// Add the menu details
			$this->menuEntries[$parent]['name'][] = $name;
			$this->menuEntries[$parent]['link'][] = $link;
			$this->menuEntries[$parent]['icon'][] = $icon;
			
			// Set the last entry
			$this->lastEntry = $name;
		}
		else if (is_null($parent))
		{
			// Show an error and die
			die("<b>PlasmaMenu Error</b>: You must add a menu before adding an entry.");
		}
		else
		{
			// Show an error and die
			die("<b>PlasmaMenu Error</b>: Menu '{$parent}' does not exist.");
		}
	}
	
	//
	// Function to add a sub menu entry
	//
	function addSubMenuEntry($name, $link, $icon = false)
	{
		// Get the parent
		$parent = $this->lastEntry;
	
		// Check if the root menu exists
		if (!is_null($parent) && $this->menuExists($parent))
		{			
			// Add the menu details
			$this->menuEntries[$parent]['name'][] = $name;
			$this->menuEntries[$parent]['link'][] = $link;
			$this->menuEntries[$parent]['icon'][] = $icon;
		}
		else if (is_null($parent))
		{
			// Show an error and die
			die("<b>PlasmaMenu Error</b>: You must add an entry before adding it's sub-menu");				
		}
		else
		{
			// Show an error and die
			die("<b>PlasmaMenu Error</b>: Sub-menu '{$parent}' does not exist.");
		}
	}		
	
	//
	// Function to add a short name
	//
	function addShortName($shortName)
	{
		// Get the menu name
		$name = $this->lastMenu;
		
		// As simple as that :-P
		if (!is_null($name))
		{
			$this->shortNames[$name] = $shortName;
		}
		else
		{
			// Show an error and die
			die("<b>PlasmaMenu Error</b>: You must add a menu before adding a short name.");
		}
	}
	
	//
	// Function to print the css
	//
	function showCss()
	{
		// Initialize some variables
		$css = '<style type="text/css">' . "\n\n";
		
		// Scan through all menus
		for ($index = 0; $index < count($this->menus['name']); $index++)
		{
			// Add the entry specific css
			$css .= $this->generateCss($index);
		}
		
		$css .= '</style>';
		
		// Let's output something now
		echo $css;
	}

	//
	// Function to print the html
	//
	function showHtml()
	{
		// Initialize some variables
		$html = '<div class="plasmamenu_box">' . "\n";
	
		// Scan through all menus
		for ($index = 0; $index < count($this->menus['name']); $index++)
		{
			// Generate the menu html
			$html .= $this->generateHtml($index);
		}
		
		$html .= '</div>';
		
		// Let's output something now
		echo $html;
	}
	
	//
	// Function to generate the HTML
	//
	function generateHtml($index)
	{
		// Get existing data
		$name = $this->menus['name'][$index];
		$safeName = $this->getSafeName($name);
		$shortName = isset($this->shortNames[$name]) ? $this->shortNames[$name] : false;
		$link = $this->menus['link'][$index];
		$icon = $this->menus['icon'][$index];
		$shadow = $this->menus['shadow'][$index];
		
		// Check if menu is empty
		if (!isset($this->menuEntries[$name]['name'][0]))
		{
			// Well, there's nothing to print..
			return;
		}
		
		// Get other data
		$imagePath = $this->imagePath;
		$iconPath = $this->iconPath;
		
		$html = "<div id=\"mainmenu_{$safeName}\" class=\"mainmenu_{$safeName}\">\n";
		$html .= "<h2>" . $name . "</h2>\n";
		$html .= "<div class=\"mainmenu_{$safeName}_sub plasmamenu_box_submenu\">\n";
		$html .= "<div class=\"menubox_top\"></div>\n";
		$html .= "<div class=\"menubox_body\">\n";
		$html .= "<div class=\"mainmenu_{$safeName}_animation\"></div>\n";
		$html .= "<div class=\"mainmenu_{$safeName}_content\">\n";
		$html .= "<ul class=\"menubox_title\"><li class=\"index\"><a " . ($link ? "href=\"{$link}\"" : null ) . ">" . ($shortName ? $shortName : $name) . "</a></li></ul>\n";
		$html .= "<img class=\"menubox_sep\" src=\"{$imagePath}menubox_hr.png\" alt=\"\" />\n";
		$html .= "<ul>\n";
		
		for ($idx = 0; $idx < count($this->menuEntries[$name]['name']); $idx++)
		{
			$entryName = $this->menuEntries[$name]['name'][$idx];
			$safeEntryName = $this->getSafeName($entryName);
			$entryLink = $this->menuEntries[$name]['link'][$idx];
			$entryIcon = $this->menuEntries[$name]['icon'][$idx];
			
			if ($this->hasChild($entryName))
			{
				$html .= "<li class=\"index mainmenu_{$safeName}_{$safeEntryName} menubox_subhover\">\n";
				$html .= $entryIcon ? "<img class=\"menubox_icon\" src=\"{$iconPath}{$entryIcon}\" alt=\"\" />" : null;
				$html .= "<h3><a " . ($entryIcon ? "class=\"menubox_space\" " : null) . ($entryLink ? "href=\"$entryLink\"" : null) . ">{$entryName}</a></h3>\n";
				$html .= "<img class=\"menubox_subarr\" src=\"{$imagePath}menubox_arrow.png\" alt=\"\" />";
				$html .= "<div class=\"mainmenu_{$safeName}_{$safeEntryName}_sub menubox_subparent\">\n";
				$html .= "<div class=\"menubox_top\"></div>\n";
				$html .= "<div class=\"menubox_body\">\n";
				$html .= "<ul>\n";				
			
				for ($subIdx = 0; $subIdx < count($this->menuEntries[$entryName]['name']); $subIdx++)
				{
					$subEntryName = $this->menuEntries[$entryName]['name'][$subIdx];
					$subEntryLink = $this->menuEntries[$entryName]['link'][$subIdx];
					$subEntryIcon = $this->menuEntries[$entryName]['icon'][$subIdx];						
				

					$html .= "<li class=\"noindex\">\n";
					$html .= $subEntryIcon ? "<img class=\"menubox_icon\" src=\"{$iconPath}{$subEntryIcon}\" alt=\"\" />" : null;
					$html .= "<a " . ($subEntryIcon ? "class=\"menubox_space\" " : null) . "href=\"{$subEntryLink}\">{$subEntryName}</a>\n";
					$html .= "</li> ";
				}

				$html .= "</ul></div><div class=\"menubox_bottom\"></div></div></li>\n";
			}
			else
			{
				$html .= "<li class=\"index\">\n";
				$html .= $entryIcon ? "<img class=\"menubox_icon\" src=\"{$iconPath}{$entryIcon}\" alt=\"\" />\n" : null;
				$html .= "<a " . ($entryIcon ? "class=\"menubox_space\" " : null) . "href=\"{$entryLink}\">{$entryName}</a>\n</li>";
			}			
		}
		$html .= "</ul></div></div><div class=\"menubox_bottom\"></div></div></div>";
		
		return $html;
	}
	
	//
	// Function to generate the CSS
	//
	function generateCss($index)
	{
		// Get existing data
		$safeName = $this->getSafeName($this->menus['name'][$index]);
		$icon = $this->menus['icon'][$index];
		$shadow = $this->menus['shadow'][$index];
	
		$css = ".mainmenu_{$safeName} { \n";
		$css .= $icon ? "background-image: url('{$this->imagePath}{$icon}'); \n" : null;
		$css .= "background-repeat: no-repeat; \n";
		$css .= "background-position: 0 0; \n";
		$css .= "position: relative; \n";
		$css .= "z-index: 95; \n";
		$css .= "}\n\n";
		
		if ($shadow)
		{
			$css .= ".mainmenu_{$safeName}:hover { \n";
			$css .= "text-shadow: {$shadow} 0px 0px 8px; \n";
			$css .= "}\n\n";
		}
		
		$css .= ".mainmenu_{$safeName}_sub { \n";
		$css .= "margin-top: 0px; \n";
		$css .= "position: absolute; \n";
		$css .= "}\n\n";
		
		$css .= ".mainmenu_{$safeName}_animation { \n";
		$css .= "position: absolute; \n";
		$css .= "left: 0px; \n";
		$css .= "opacity: 0; \n";
		$css .= "}\n\n";		
		
		return $css;
	}
	
	// 
	// Function to return a id safe name
	//
	function getSafeName($name)
	{
		return strtolower(preg_replace("/\s/", "_", $name));
	}
	
	//
	// Function to check if a menu exists
	//
	function menuExists($name)
	{
		// Recurse through all root menus
		foreach($this->menus['name'] as $menuName)
		{
			if (strcmp($menuName, $name) == 0)
			{
				// Yes, it exists! It's a root menu
				return true;
			}
			
			// Check for submenus
			if (isset($this->menuEntries[$menuName]['name']) && 
				is_array($this->menuEntries[$menuName]['name']))
			{
				foreach($this->menuEntries[$menuName]['name'] as $subMenuName)
				{
					if (strcmp($subMenuName, $name) == 0)
					{
						// It's a submenu
						return true;
					}				
				}
			}
		}	
		
		// Menu doesn't exist
		return false;
	}
	
	//
	// Function to check if a menu is a root level menu
	//
	function isRootMenu($name)
	{
		// Recurse through all root menus
		foreach($this->menus['name'] as $menuName)
		{
			if (strcmp($menuName, $name) == 0)
			{
				// It is a root menu
				return true;
			}
		}	

		// It isn't
		return false;
	}
	
	//
	// Function to check if a menu entry has a child menu
	//
	function hasChild($name)
	{
		// If there is atleast one entry with that parent
		if (isset($this->menuEntries[$name]['name'][0]))
		{
			// Yea, it has a child
			return true;
		}
		
		// It doesn't
		return false;
	}

	//
	// Function to generate a breadcrumb
	//
	function breadCrumb()
	{
      global $site_title;
		// Get the current request URL
		$url = $_SERVER["REQUEST_URI"];
		
		// Get the separator
		$sep = $this->breadcrumbSep;
		
		// TEMPORARY
		$url = preg_replace("/mainsite\//", "", $url);
		
		// Init the breadcrumb
		$nav = $this->buildLink($site_title, "/");
		
		// If the user is on the root
		if ($url == "/")
		{
			echo $nav;
			return;
		}
		
		// Boolean flag
		$set = false;
		
		// Recurse through all entries
		for ($idx = 0; $idx < count($this->menus['name']); $idx++)
		{
			$name = $this->menus['name'][$idx];
			$link = $this->menus['link'][$idx];
			
			for ($entIdx = 0; $entIdx < count($this->menuEntries[$name]['name']); $entIdx++)
			{
				$entryName = $this->menuEntries[$name]['name'][$entIdx];
				$entryLink = $this->menuEntries[$name]['link'][$entIdx];
				
				if ($this->hasChild($entryName))
				{
					for ($subIdx = 0; $subIdx < count($this->menuEntries[$entryName]['name']); $subIdx++)
					{
						$subEntryName = $this->menuEntries[$entryName]['name'][$subIdx];
						$subEntryLink = $this->menuEntries[$entryName]['link'][$subIdx];
						
						if ($url && $subEntryLink && strpos($url, $subEntryLink) !== false && !$set)
						{
							$nav .= $sep . $this->buildLink($name, $link);
							$nav .= $sep . $this->buildLink($entryName, $entryLink);
							$nav .= $sep . i18n_var( $subEntryName );
							$set = true;
						}
					}
				}
				
				if ($url && $entryLink && strpos($url, $entryLink) !== false && !$set)
				{
					$nav .= $sep . $this->buildLink($name, $link);
					$nav .= $sep . i18n_var( $entryName );
					$set = true;
				}					
			}
			
			if ($url && $link && strpos($url, $link) !== false && !$set)
			{
				$nav .= $sep . $name;
			}			
		}
		
		echo $nav;
	}
	
	//
	// Function to build a hyperlink
	//
	function buildLink($name, $url)
	{
		// Return the link
		return ($url ? "<a href=\"{$url}\">".i18n_var($name)."</a>" : i18n_var( $name ) );
	}	
}

?>
