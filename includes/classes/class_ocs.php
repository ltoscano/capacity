<?php

/**
* OCS Lib
*
* @author Frank Karlitschek 
* @copyright 2010 Frank Karlitschek karlitschek@kde.org 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
* License as published by the Free Software Foundation; either 
* version 3 of the License, or any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU AFFERO GENERAL PUBLIC LICENSE for more details.
*  
* You should have received a copy of the GNU Lesser General Public 
* License along with this library.  If not, see <http://www.gnu.org/licenses/>.
* 
*/



class OCS {

  static function importall(){

    $categories =array(
      '1'=>array('name'=>'Themes','ids'=>'1x2x3x4x5x6x7x8x9x10x11x12x13x14x15x16x17x18x19x20x21x22x23x24x25x26x27x28x29x30x31x32x34x35x36x37x38x39x40x41x42x43x44x45x55x60x61x62x63x64x65x66x67x68x70x71x72x73x74x75x76x77x78x79x80x81x102x103x287'),
      '2'=>array('name'=>'Development','ids'=>'260x261'),
      '3'=>array('name'=>'Education','ids'=>'242'),
      '4'=>array('name'=>'Games','ids'=>'250x251x252x253x254'),
      '5'=>array('name'=>'Graphics','ids'=>'222x223x224'),
      '6'=>array('name'=>'Internet','ids'=>'230x231x232x233x234x235x236'),
      '7'=>array('name'=>'Multimedia','ids'=>'220x221x56x57x58'),
      '8'=>array('name'=>'Office','ids'=>'210x211x212x213x214'),
      '9'=>array('name'=>'System','ids'=>'270x271x272x273x281'),
      '10'=>array('name'=>'Utilities','ids'=>'282x284x271x285'),
      '11'=>array('name'=>'All','ids'=>'210x211x212x213x214x220x221x56x57x58x222x223x224x230x231x232x233x234x235x236x250x251x252x253x254x260x261x260x261x270x271x272x273x281x282x284x271x285')
    );
   
    foreach($categories as $key=>$value) {
      OCS::import($key,$value);
    } 

  }

  static function import($category,$ocscategories){
    $url='http://api.opendesktop.org/v1/content/data?categories='.$ocscategories['ids'].'&sortmode=new&page=0&pagesize=10';

    $xml=file_get_contents($url);
    $data=simplexml_load_string($xml);

    // remove old stuff
    $request=DB::query('delete from ocs where category="'.addslashes($category).'"');
    DB::free_result($request);


    $tmp=$data->data->content;
    for($i = 0; $i < count($tmp); $i++) {

      $request=DB::query('insert into ocs (category,name,type,user,url,preview,timestamp,description) values("'.$category.'","'.addslashes($tmp[$i]->name).'", "'.addslashes($tmp[$i]->typeid).'","'.addslashes($tmp[$i]->personid).'","'.addslashes($tmp[$i]->detailpage).'", "'.addslashes($tmp[$i]->smallpreviewpic1).'","'.addslashes(strtotime($tmp[$i]->changed)).'","'.addslashes($tmp[$i]->description).'"  ) ');
      DB::free_result($request);

    } 





  }




  static function show($category,$count){
    global $kde_contributors;

    $dateformat="M j G:i";

    $sql='select name,type,user,url,preview,timestamp,description from ocs where category='.$category.' order by timestamp desc limit '.$count;
    $request=DB::query($sql);
    $num=DB::numrows($request);
    echo('<div class="table-wrapper"><table class="ocs">');
    for($i = 0; $i < $num; $i++) {
      echo('<tr><td class="ocs-applicationshot">');
      $data=DB::fetch_assoc($request);
      if(isset($data['preview']) and !empty($data['preview'])) echo('<a href="'.$data['url'].'"><img src="'.$data['preview'].'" alt="'.$data['name'].'" title="'.$data['name'].'" /></a>');

      echo('</td><td class="ocs-latestapp">');
      echo('<span class="ocs-title"><a href="'.$data['url'].'">'.$data['name'].'</a></span><br />');
      if(!empty($data['description'])) echo(''.substr($data['description'],0,200).' ...');
      echo('</td></tr>');
    }
    echo('</table></div>');
    DB::free_result($request);

  }





}


?>
