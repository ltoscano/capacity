<?php
/**
* Chihuahua theme for Capacity
* @ver 0.1
* @license BSD License - www.opensource.org/licenses/bsd-license.php
*
* Copyright (c) 2012 Ingo Malchow <imalchow@kde.org>
* All rights reserved. Do not remove this copyright notice.
*/

// This file contains content between the top of the html body and the heading
?>
    <div class="root">
    	<div class="header">
    		<div class="toolbox">
    		 <?php  include  $templatepath.'toolbox.inc'; ?>
			</div>
			<?php
			  $template_menulist1 = "<li>";  // before the menu section title
			  $template_menulist2 = "<div class=\"sub\">";  // between the menu section title and the list of pages
			  $template_menulist3 = "</div></li>"; // after the list of pages

			  $template_menusublist1 = ""; // before each link to a (sub)page with shown (sub)subpages
			  $template_menusublist2 = ""; // between the link to a (sub)page and its list of (sub)subpages
			  $template_menusublist3 = ""; // after each list of subpages

			  $template_menuitem1 = ""; // before each link to a (sub)page without shown (sub)subpages
			  $template_menuitem2 = ""; // after each link to a (sub)page without shown (sub)subpages

			  if ($site_menus > 0 && $templatepath == "chihuahua/") 
			  {
				if (isset($site) && $site == "developer")
				{
					echo '<div class="menu_box" onmouseover="javascript: $(\'.sub\').show(\'slow\');">
							<ul>';
							
					echo $template_menulist1."\n";
					$kde_menu->currentMenu();
					echo $template_menulist3."\n";

					if ($kde_menu->activeSection () != 0) {
						echo $template_menulist1."\n";
						$kde_menu->currentMenu(true);
						echo $template_menulist3."\n";
					}
					
					echo '</div>';
				}
				else
					$plasmaMenu->showHtml();
			  }
			?>
		</div>
	<div class="content">
	<script type="text/javascript" src="/media/javascripts/jquery-1.4.2.min.js"></script>
   <script type="text/javascript" src="/media/javascripts/jquery.mailme.js"></script>
	<script type="text/javascript" src="/media/javascripts/plasmaMenu.min.js"></script>	
	<script type="text/javascript" src="/media/javascripts/jquery.cycle.all.min.js"></script>
	<script type="text/javascript">
		(function($) {
			var cache = [];
			// Arguments are image paths relative to the current page.
			$.preLoadImages = function() {
			    var args_len = arguments.length;
			    for (var i = args_len; i--;) {
				var cacheImage = document.createElement('img');
				cacheImage.src = arguments[i];
				cache.push(cacheImage);
			    }
			 }
		})(jQuery)
		
		$(document).ready(function() {
			$('span.mailme').mailme();
			$('.sub').hide();
			$('.toggle').show();
			$('.teaser_hide').css('display', 'block');
			<?php 
			if (isset($teaser) && $teaser) { 
				// initialize the teaser class and it's object
				include_once("classes/class_teaser.inc");
				$teaserImages = new teaserImageBuilder();
				if (isset($teaserFiles)) {
					$teaserImages->files = $teaserFiles;
				}
			?>
			// You need to add a comma after the php function output if there are more entries
			jQuery.preLoadImages(<?php $teaserImages->imageList(); ?>,
								 "/media/images/plasmaMenu/menubox_top.png", 
								 "/media/images/plasmaMenu/menubox_body.png",
								 "/media/images/plasmaMenu/menubox_bottom.png");
			$('.teaser').cycle({
				fx: '<?php echo $teaserImages->options['fx']; ?>',
				timeout: <?php echo $teaserImages->options['timeout']; ?>,
				speed: <?php echo $teaserImages->options['speed']; ?>,
				next : $('#next'),
				prev : $('#previous')
			});

			var show = function(e) {
				var teaserPosition = $('.teaser').offset();
				var next = $('#next');
				var prev = $('#previous');
				var left = (teaserPosition.left + $('.teaser').width()) / 2;
				next.css({top:teaserPosition.top, left:left}).show();
				prev.css({top:teaserPosition.top+$('.teaser').height()-prev.height(), left:left}).show();
			};

			var hide = function(time) {
				$('#next').hide();
				$('#previous').hide();
			};

			var pause = function() {
				$('.teaser').cycle('pause');
			}

			var resume = function() {
				$('.teaser').cycle('resume');
			}

			$('.teaser').mouseover(show);
			$('.teaser').mouseout(hide);
			$('.teaser').mouseover(pause);
			$('.teaser').mouseout(resume);

			$('#next').mouseover(show);
			$('#previous').mouseover(show);
			
			<?php } ?>
		});
	</script><?php if (isset($teaser) && $teaser) $teaserImages->generate(); ?>
    <div id="main">
        <?php
            if (isset($site_showkdeevdonatebutton) && $site_showkdeevdonatebutton && (!isset($page_disablekdeevdonatebutton) || !$page_disablekdeevdonatebutton))
            {
        ?>
               <div style="float: right; padding: 7px 10px; margin-left: 10px; border: 1px solid #CCC; border-radius: 12px;" >

                    <div style='font-size: 14px; color: #909090; float: left; padding-top: 5px;'><?php i18n("DONATE");?> <a href='//www.kde.org/community/donations/index.php#money'><?php i18n("(Why?)");?></a></div>
                    <img src='/media/images/paypal.png' style='padding: 0px; border: none; margin: 0px; margin-left: 10px; float: right;' alt="paypal" />
                    <form style='clear: both; margin: 0px; box-shadow: none; padding: 0px; padding-top: 10px; border: none;' action="https://www.paypal.com/en_US/cgi-bin/webscr" method="post" onsubmit="return amount.value >= 2 || window.confirm('<?php print i18n_var("Your donation is smaller than %1€. This means that most of your donation\\nwill end up in processing fees. Do you want to continue?", 2);?>');">
                        <input type="hidden" name="cmd" value="_donations" />
                        <input type="hidden" name="lc" value="GB" />
                        <input type="hidden" name="item_name" value="Development and communication of KDE software" />
                        <input type="hidden" name="custom" value="<?php print htmlspecialchars("//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]/donation_box"); ?>" />
                        <input type="hidden" name="currency_code" value="EUR" />
                        <input type="hidden" name="cbt" value="Return to www.kde.org" />
                        <input type="hidden" name="return" value="https://www.kde.org/community/donations/thanks_paypal.php" />
                        <input type="hidden" name="notify_url" value="https://www.kde.org/community/donations/notify.php" />
                        <input type="hidden" name="business" value="kde-ev-paypal@kde.org" />
                        <input type='text' name="amount" value="20.00" style='text-align: right; padding-right: 1em; border: 1px solid #CCC; width: 60px; height: 23px; ' /> €
                        <button style='cursor: pointer; background-color: #0070BB; border: 1px solid #0060AB; color: #FFF; height: 27px;' type='submit'><?php i18n("Donate");?></button>
                    </form>
                </div>
        <?php
        }
        ?>

<?php
    if (isset($site_languages) && count($site_languages) > 1)
    {
?>    
          <div id="languageChooser">
             <form method="get" name="languagecombo" action="">
                <select name="site_locale">
                    <?php
                        foreach ($site_languages as &$value) {
                            if ($value == $site_locale) $selected=" selected='selected'";
                            else $selected="";
                            print "<option value='".$value."'".$selected.">".languageName($value)."</option>";
                        }
                    ?>
                </select>
                <input type="submit" value=" <?php i18n("Change language")?> " id="languageButton" />
             </form>
          </div>
<?php
}
?>
