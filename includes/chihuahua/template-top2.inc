<?php 
/**
* Chihuahua theme for Capacity
* @ver 0.1
* @license BSD License - www.opensource.org/licenses/bsd-license.php
*
* Copyright (c) 2012 Ingo Malchow <imalchow@kde.org>
* All rights reserved. Do not remove this copyright notice.
*/

    if( isset($sidebar_content) && $sidebar_content != "" ) { ?>
        <div id="sidebar">
            <?php global $sidebar_content; echo $sidebar_content; ?>
        </div>
    <?php } ?>
    <div id="module">
    </div>
