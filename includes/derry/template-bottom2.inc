<?php // This file contains content between the menu and the end of html body

  // figure out the contact
  if (isset($name) && isset($mail))
    $contact = i18n_var("Maintained by") . " <a href=\"mailto:$mail\">$name</a><br />\n";
  else
    $contact = i18n_var("Maintained by <a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#x77;e&#098;ma&#115;t&#x65;&#x72;&#64;kde&#46;or&#x67;\">The KDE Webmaster</a><br />\n");
?>

    <div id="footer"><div id="footer_text">
        <?php print $contact; ?>
<?php i18n('KDE<sup>&#174;</sup> and <a href="/media/images/trademark_kde_gear_black_logo.png">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href="http://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a>');?> |
        <a href="http://www.kde.org/contact/impressum.php"><?php i18n("Legal")?></a>
    </div></div>

<!--
WARNING: DO NOT SEND MAIL TO THE FOLLOWING EMAIL ADDRESS! YOU WILL
BE BLOCKED INSTANTLY AND PERMANENTLY!
<?php
  $trapmail = "aaaatrap-";
  $t = pack('N', time());
  for($i=0;$i<=3;$i++) {
      $trapmail.=sprintf("%02x",ord(substr($t,$i,1)));
  }
  $ip=$_SERVER["REMOTE_ADDR"];
  sscanf($ip,"%d.%d.%d.%d",$ip1,$ip2,$ip3,$ip4);
  $trapmail.=sprintf("%02x%02x%02x%02x",$ip1,$ip2,$ip3,$ip4);

  echo "<a href=\"mailto:$trapmail@kde.org\">Block me</a>\n";
?>
WARNING END
-->

<?php
  if (isset ($_GET ['javascript'])) {
?>

<script type="text/javascript">
function fitFooter() {
  try {
    document.getElementById ('body').style.minHeight = (window.innerHeight - document.getElementsByTagName ('body')[0].offsetHeight + document.getElementById ('body').offsetHeight) + "px";
    window.setTimeout ("fitFooter()", 200);
  } catch (e) {
  }
};
window.setTimeout ("fitFooter()", 100);
</script>

<?php
}
?>
