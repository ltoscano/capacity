  <div id="location" class="toolboxtext">
<?php
    if (isset($site_languages) && count($site_languages) > 1)
    {
?>    
             <form method="get" name="languagecombo" action="">
                <select name="site_locale">
                    <?php
                        foreach ($site_languages as &$value) {
                            if ($value == $site_locale) $selected=" selected='selected'";
                            else $selected="";
                            print "<option value='".$value."'".$selected.">".languageName($value)."</option>";
                        }
                    ?>
                </select>
                <input type="submit" value=" <?php i18n("Change language")?> " id="languageButton" />
             </form>
<?php
}
if (! isset ($hidelocation) || $hidelocation == false) { ?>
    <ul>
      <li><?php $plasmaMenu->breadCrumb() ?></li>
    </ul>
<?php } ?>
  </div>
