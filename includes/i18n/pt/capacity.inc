<?php
$text["Maintained by"] = "Mantido por";
$text["Maintained by <a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#x77;e&#098;ma&#115;t&#x65;&#x72;&#64;kde&#46;or&#x67;\">The KDE Webmaster</a><br />\n"] = "Mantido pelo <a href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#x77;e&#098;ma&#115;t&#x65;&#x72;&#64;kde&#46;or&#x67;\">Administrador Web do KDE</a><br />\n";
$text["Search:"] = "Procurar:";
$text["Search"] = "Procurar";
$text["Language:"] = "Língua:";
$text["Change language"] = "Mudar a língua";
$text["Donate:"] = "Doação:";
$text["(Why?)"] = "(Porquê?)";
$text["Your donation is smaller than %1€. This means that most of your donation\\nwill end up in processing fees. Do you want to continue?"] = "A sua doação é inferior a %1€. Isto significa que a maior parte da sua doação\\nirá ser destinada a taxas de processamento. Deseja realmente continuar?";
$text["Donate"] = "Doar";
$text["KDE<sup>&#174;</sup> and <a href=\"/media/images/trademark_kde_gear_black_logo.png\">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of "] = "O KDE<sup>&#174;</sup> e o <a href=\"/media/images/trademark_kde_gear_black_logo.png\">logótipo do Ambiente de Trabalho K<sup>&#174;</sup></a> são marcas registadas de ";
$text["Legal"] = "Legal";
$text["KDE<sup>&#174;</sup> and <a href=\"/media/images/trademark_kde_gear_black_logo.png\">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href=\"http://ev.kde.org/\" title=\"Homepage of the KDE non-profit Organization\">KDE e.V.</a>"] = "O KDE<sup>&#174;</sup> e o <a href=\"/media/images/trademark_kde_gear_black_logo.png\">logótipo do Ambiente de Trabalho K<sup>&#174;</sup></a> são marcas registadas da <a href=\"http://ev.kde.org/\" title=\"Página da Organização sem Fins Lucrativos do KDE\">KDE e.V.</a>";
$text["DONATE"] = "DOAR";
$text["%1 is unmaintained and no longer released by the KDE community."] = "O %1 não está a ser mantido e não é mais providenciado pela comunidade do KDE.";
$text["Authors:"] = "Autores:";
$text["Thanks To:"] = "Agradecimentos a:";
$text["%1 is distributed under the terms of the <a href=\"http://www.gnu.org/licenses/old-licenses/gpl-2.0.html\">GNU General Public License (GPL), Version 2</a>."] = "O %1 é distribuído segundo os termos da <a href=\"http://www.gnu.org/licenses/old-licenses/gpl-2.0.html\">Licença Pública Geral da GNU (GPL), Versão 2</a>.";
$text["%1 is distributed under the terms of the <a href=\"http://www.gnu.org/licenses/gpl.html\">GNU General Public License (GPL), Version 3</a>."] = "O %1 é distribuído segundo os termos da <a href=\"http://www.gnu.org/licenses/gpl.html\">Licença Pública Geral da GNU (GPL), Versão 3</a>.";
$text["%1 is distributed under the terms of the <a href=\"http://www.gnu.org/licenses/old-licenses/lgpl-2.0.html\"> GNU Library General Public License, version 2</a>."] = "O %1 é distribuído segundo os termos da <a href=\"http://www.gnu.org/licenses/old-licenses/lgpl-2.0.html\">Licença Pública Geral Lata da GNU (LGPL), Versão 2</a>.";
$text["Browse %1 source code on WebSVN"] = "Navegar pelo código-fonte do %1 no WebSVN";
$text["Browse %1 source code online"] = "Navegar pelo código-fonte do %1 'online'";
$text["Checkout source code:"] = "Obter o código-fonte:";
$text["Clone %1 source code:"] = "Clonar o código-fonte do %1:";
$text["%1 is distributed under the terms of the <a href=\"http://www.gnu.org/licenses/gpl.html\">GNU General Public License (GPL), Version 2</a>."] = "O %1 é distribuído segundo os termos da <a href=\"http://www.gnu.org/licenses/gpl.html\">Licença Pública Geral da GNU (GPL), Versão 2</a>.";
$text["%1 is distributed under the terms of the <a href=\"http://www.gnu.org/licenses/lgpl.html\">GNU Lesser General Public License (LGPL), Version 2</a>."] = "O %1 é distribuído segundo os termos da <a href=\"http://www.gnu.org/licenses/lgpl.html\">Licença Pública Geral Lata da GNU (LGPL), Versão 2</a>.";
$text["Unknown license, valid licenses for setLicense() are 'gpl', 'lgpl'. Update www/media/includes/classes/class_appinfo.inc if another is required."] = "A licença é desconhecida; as licenças válidas para o 'setLicense()' são 'gpl', 'lgpl'. Actualize o ficheiro 'www/media/includes/classes/class_appinfo.inc' se for necessária outra.";
$text["<b>Version %1</b>"] = "<b>Versão %1</b>";
$text["Questions"] = "Perguntas";
$text["Answers"] = "Respostas";
$text["[Up to Questions]"] = "[Ir para as Perguntas]";
$text["Skip menu \"%1\""] = "Saltar o menu \"%1\"";
$text["Latest News"] = "Últimas Notícias";
$text["Date"] = "Data";
$text["Headline"] = "Destaque";
$text["This is a list of the latest news headlines shown on KDE Dot News"] = "Esta é uma lista com os últimos destaques de notícias presentes nas KDE Dot News";
$text["Read the full article on dot.kde.org"] = "Leia o artigo completo em dot.kde.org";
$text["View older news items"] = "Ver os itens mais antigos de notícias";
$text["more news..."] = "mais notícias...";
$text["Latest Applications"] = "Últimas Aplicações";
$text["List of latest third party application releases for KDE"] = "Lista das últimas versões de aplicações de terceiros para o KDE";
$text["Application / Release"] = "Aplicação / Versão";
$text["View <a href=\"http://kde-apps.org/\">more applications...</a>"] = "Veja <a href=\"http://kde-apps.org/\">mais aplicações...</a>";
$text["Disclaimer: "] = "Chamada de atenção: ";
$text["Application feed provided by <a href=\"http://kde-apps.org/\">kde-apps.org</a>, an independent KDE website."] = "Fonte da aplicação fornecida pelo <a href=\"http://kde-apps.org/\">kde-apps.org</a>, uma página Web independente do KDE.";
$text["Global navigation links"] = "Itens de navegação global";
$text["KDE Home"] = "Página Inicial do KDE";
$text["KDE Accessibility Home"] = "Página de Acessibilidade do KDE";
$text["Description of Access Keys"] = "Descrição das Teclas de Acesso";
$text["Back to content"] = "Voltar ao conteúdo";
$text["Back to menu"] = "Voltar ao menu";
$text["K Desktop Environment"] = "Ambiente de Trabalho K";
$text["Skip to content"] = "Ir para o conteúdo";
$text["Skip to link menu"] = "Ir para o menu de ligações";
$text["Y-m-d"] = "d-m-Y";
$text["Select what or where you want to search"] = "Seleccione o quê e onde procurar";
$text["kde.org"] = "kde.org";
$text["developer.kde.org"] = "developer.kde.org";
$text["kde-look.org"] = "kde-look.org";
$text["Applications"] = "Aplicações";
$text["Documentation"] = "Documentação";
$text["Hotspot"] = "Ponto fulcral";
$text["KDE<sup>&#174;</sup> and <a href=\"/media/images/kde_gear_black.png\">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href=\"http://ev.kde.org/\" title=\"Homepage of the KDE non-profit Organization\">KDE e.V.</a>"] = "O KDE<sup>&#174;</sup> e o <a href=\"/media/images/kde_gear_black.png\">logótipo do Ambiente de Trabalho K<sup>&#174;</sup></a> são marcas registadas da <a href=\"http://ev.kde.org/\" title=\"Página da Organização sem Fins Lucrativos do KDE\">KDE e.V.</a>";
$text["Choose one of the local KDE sites or mirror"] = "Escolha uma das páginas ou réplicas locais do KDE";
$text["Choose your location"] = "Escolha a sua localização";
$text["KDE in Your Country"] = "O KDE no seu País";
$text["Americas"] = "Américas";
$text["Brasil"] = "Brasil";
$text["Chile"] = "Chile";
$text["Asia"] = "Ásia";
$text["China"] = "China";
$text["Iran"] = "Irão";
$text["Israel"] = "Israel";
$text["Japan"] = "Japão";
$text["Korea"] = "Coreia";
$text["Taiwan"] = "Formosa";
$text["Turkey"] = "Turquia";
$text["Europe"] = "Europa";
$text["Bulgaria"] = "Bulgária";
$text["Czech Republic"] = "República Checa";
$text["France"] = "França";
$text["Germany"] = "Alemanha";
$text["Great Britain"] = "Grã-Bretanha";
$text["Hungary"] = "Hungria";
$text["Iceland"] = "Islândia";
$text["Ireland"] = "Irlanda";
$text["Italy"] = "Itália";
$text["Netherlands"] = "Holanda";
$text["Poland"] = "Polónia";
$text["Romania"] = "Roménia";
$text["Russia"] = "Rússia";
$text["Spain"] = "Espanha";
$text["Ukraine"] = "Ucrânia";
$text["Yugoslavia"] = "Jugoslávia";
$text["Regional KDE Mirrors"] = "Réplicas Regionais do KDE";
$text["United States"] = "Estados Unidos";
$text["Malaysia"] = "Malásia";
$text["Thailand"] = "Tailândia";
$text["Australia &amp; Oceans"] = "Austrália &amp; Oceânia";
$text["Australia"] = "Austrália";
$text["Austria"] = "Áustria";
$text["Greece"] = "Grécia";
$text["Lithuania"] = "Lituânia";
$text["Portugal"] = "Portugal";
$text["Slovenia"] = "Eslovénia";
$text["Sweden"] = "Suécia";
$text["Switzerland"] = "Suíça";
$text["Go"] = "Ir";
$text["A complete structural overview of the KDE.org web pages"] = "Uma visão estrutural completa das páginas Web do kde.org";
$text["Sitemap"] = "Mapa do 'Site'";
$text["Having problems? Read the documentation"] = "Tem algum problema? Leia a documentação";
$text["Help"] = "Ajuda";
$text["Contact information for all areas of KDE"] = "Informação de contacto para todas as áreas do KDE";
$text["Contact Us"] = "Contacte-Nos";
$text["Location"] = "Localização";
?>

