<?php
$hotspot = new Hotspot();
//
// Format of the hotspot entries:
//	Each entry gets a comment stating how long the hotspot
//	should run; use an end-date. Hotspots are kept sorted
//	by expiry (earliest first).
//
//	@param imagepath  path to image
//	@param URL        URL to link to
//	@param label      (optional) text to display below image
//
// To keep the hotspot from being boring, we keep around some 
// spares which are always OK to use, like kde-look.org .
// Add them in if needed or if there's nothing else.
//

//$hotspot->add("/media/images/hotspots/10years.png", "http://dot.kde.org/1160834616/");

//$hotspot->add("/media/images/hotspots/koffice1.6.png", "http://www.koffice.org/");

//$hotspot->add("/media/images/hotspots/lugradiolive-usa-2008.jpg", "http://lugradio.org/live/USA2008/");

// Spares if there's nothing exciting going on.
//
$hotspot->add("/media/images/hotspots/kde-supporting-members.png", "http://ev.kde.org/supporting-members.php");
$hotspot->add("/media/images/hotspots/kde-techbase.png", "http://techbase.kde.org/");
// $hotspot->add("/media/images/hotspots/kde-look.png", "http://kde-look.org/", "KDE-Look");
// $hotspot->add("/media/images/hotspots/kde-cws_hotspot.png", "http://wiki.kde.org/","KDE Wiki");

$hotspot->show();
?>
