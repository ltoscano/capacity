    <meta charset="UTF-8"/>
    <title><?= $site_title ?></title>
    <meta name="description" content="K Desktop Environment Homepage, KDE.org" />
    <meta name="robots" content="all" />
    <meta name="no-email-collection" content="http://www.unspam.com/noemailcollection" />
    <link rel="stylesheet" href="https://cdn.kde.org/aether-devel/bootstrap.css" />
    <link rel="stylesheet" href="https://cdn.kde.org/aether-devel/aether-kde-org.css" />
<?php
  if (file_exists("$site_root/favicon.ico")) {
    print "<link rel=\"shortcut icon\" href=\"$site_root/favicon.ico\" />";
    print "<link rel=\"icon\" href=\"$site_root/favicon.ico\" />";
  } else {
    print "<link rel=\"icon\" href=\"/media/images/favicon.ico\" />";
    print "<link rel=\"shortcut icon\" href=\"/media/images/favicon.ico\" />";
  }

  if (isset($rss_feed_link))
    print "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"$rss_feed_title\" href=\"$rss_feed_link\" />\n";
  if (isset($rss_feed2_link))
    print "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"$rss_feed2_title\" href=\"$rss_feed2_link\" />\n";

  $cssparameters = "";
?>
