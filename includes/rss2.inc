<?php

// Copyright (C) 2004 Nicolas GOUTTE: goutte at kde dot org

// Original copyright of functions.inc
/*
 * Filename: 	functions.php
 * Description: supplies functions for the KDE Homepage.
 * Functions: 	printNews(items)
 * Author: 	Sebastian Faubel
 * Modified by: Jason Bainbridge and Chris Howells
 * Comments: Added some caching code for the rdf generation.
 * NB. Those functions should be merged together at sometime
 *     for the sake of cleaner code
 *	   + the html needs to be cleaned up
 */

/*
 * Function for news RDF file, when RDF file is a validable RSS2 file.
 *
 * Changes compared to kde_general_news's RSS:
 * - RFC 822 date
 * - other tag names
 * - <description> can have escaped HTML code
 */

function kde_general_news_rss2 ($file, $items, $summaryonly, $useexternallinks = false, $summaryonlytitle = "")
{
  global $site_locale;
  startTranslation($site_locale);
  
  if ($summaryonly)
  {
    if ($summaryonlytitle)
      print "<h2><a name=\"news\">" . $summaryonlytitle . "</a></h2>\n";
    else
      print "<h2><a name=\"news\">" . i18n_var("Latest News") . "</a></h2>\n";
  }

  $news = new RDF();
  $rdf_pieces = $news->openRDF($file);

  if(!$items)
  {
     $items = 5; // default
  }
  $rdf_items = count($rdf_pieces);
  if ($rdf_items > $items)
  {
     $rdf_items = $items;
  }

  //only open the file if it has something in it
  if ($rdf_items > 0)
  {
    /* Don't display the last story twice
    * if there is less than the requested number of stories
    * in the RDF file */
    if ($rdf_items < $items)
    {
      $rdf_items = $rdf_items - 1;
    }

    print "<table width=\"100%;\" cellpadding=\"6\">\n";

    if ($summaryonly)
      print "<tr><th>" . i18n_var("Date") . "</th>\n<th>" . i18n_var("Headline") . "</th></tr>\n";
    
    for($x=1;$x<=$rdf_items;$x++)
    {
      preg_match("@<title>(.*)</title>@s", $rdf_pieces[$x], $title);
      preg_match("@<link>(.*)</link>@s",$rdf_pieces[$x], $links);
      preg_match("@<pubDate>(.*)</pubDate>@s", $rdf_pieces[$x], $date);
      preg_match("@<description>(.*)</description>@s", $rdf_pieces[$x], $description);
      print "<tr>\n";

      $parsedDate = strtotime( $date[1] );
      if ( $parsedDate === -1 )
      {
          // Date could not be parsed, so give back the raw date
          $cookedDate = $date[1];
      }
      else
      {
          $cookedDate = gmdate( i18n_var("Y-m-d"), $parsedDate );
      }
      
      if ($summaryonly)
      {
        print "<td valign=\"top\">".(($cookedDate == $prevDate) ? "&nbsp;" : "<b>$cookedDate</b>")."</td>\n";
        if ($useexternallinks) print "<td><a href=\"$links[1]\">$title[1]</a></td>\n";
        else print "<td><a href=\"news.php#item" . preg_replace("/[^a-zA-Z0-9]/", "", $title[1]) . "\">$title[1]</a></td>\n";
        $prevDate=$cookedDate;
      }
      else
      {
        // We need to unescape the XML/HTML character references in the <description>
        $desc = $description[1];
        $desc = preg_replace("/&lt;/","<", $desc);
        $desc = preg_replace("/&gt;/",">", $desc);
        $desc = preg_replace("/&amp;/","&", $desc);
        print "<td><h3><a name=\"item" . preg_replace("[^a-zA-Z0-9]", "", $title[1]) . "\">$cookedDate: $title[1]</a></h3></td>\n";
        print "</tr><tr><td>$desc</td>\n";
      }
      print "</tr>\n";
    }
    print "</table>\n";
  }
}

?>
