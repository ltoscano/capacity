<?php

/**
* Microblogging Lib
*
* @author Frank Karlitschek 
* @copyright 2010 Frank Karlitschek karlitschek@kde.org 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
* License as published by the Free Software Foundation; either 
* version 3 of the License, or any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU AFFERO GENERAL PUBLIC LICENSE for more details.
*  
* You should have received a copy of the GNU Lesser General Public 
* License along with this library.  If not, see <http://www.gnu.org/licenses/>.
* 
*/



class MICROBLOG {


  static function gettimeline($mode,$userid,$twitterid){

    if($mode=='twitter') { 
      $url='https://api.twitter.com/1/statuses/user_timeline.rss?screen_name='.$twitterid;
      $homepage='http://twitter.com/';
    }else{
      $url='http://identi.ca/api/statuses/user_timeline/'.$twitterid.'.xml';
      $homepage='http://identi.ca/';
    }
    $xml='';
    $result=fopen($url,'r');	
    if(!$result){
      echo('connection error to: '.$url."\n");
    }else{
      while (!feof($result)) {
        $xml .= fread($result, 8192);
      }
      fclose($result);
    }

    $data=@simplexml_load_string($xml);

    if(!$data){
      echo('parse error: '.$url."\n");
    }else{

      if(count($data->status)>0){
        $request=DB::query('delete from microblog where user="'.addslashes($userid).'" and mode="'.$mode.'"');
        DB::free_result($request);

        foreach($data->status as $item){
          $request=DB::query('insert into microblog (mode,user,message,url,timestamp) values("'.$mode.'","'.addslashes($userid).'","'.addslashes($item->text).'","'.addslashes($homepage.$twitterid).'","'.strtotime($item->created_at).'") ');
          DB::free_result($request);
        }
      }
    }
  }




  static function show($count){
    global $kde_contributors;

    $dateformat="M j G:i";

    $request=DB::query('select user,message,url,timestamp from microblog order by timestamp desc limit '.$count);
    $num=DB::numrows($request);
    echo('<div class="table-wrapper"><table class="ocs">');
    for($i = 0; $i < $num; $i++) {
      echo('<tr><td class="ocs-hackergotchi">');
      $data=DB::fetch_assoc($request);
      if(isset($kde_contributors[$data['user']]['picture'])) echo('<img src="/userpictures/'.$kde_contributors[$data['user']]['picture'].'" align="left" alt="'.$kde_contributors[$data['user']]['name'].'" title="'.$kde_contributors[$data['user']]['name'].'" />');
      echo('</td><td class="ocs-content">');

      echo('<a href="'.$data['url'].'">'.$data['message'].'</a>');

      echo('</td><td class="ocs-linksbar">');
      if(isset($kde_contributors[$data['user']]['twitter']) and !empty($kde_contributors[$data['user']]['twitter'])) echo('<a href="http://twitter.com/'.$kde_contributors[$data['user']]['twitter'].'"><img hspace="5" src="/images/pics/twitter.png" border="0" alt="Twitter" title="Twitter" /></a>'); 
      if(isset($kde_contributors[$data['user']]['identica']) and !empty($kde_contributors[$data['user']]['identica'])) echo('<a href="http://identi.ca/'.$kde_contributors[$data['user']]['identica'].'"><img hspace="5" src="/images/pics/identica.png" border="0" alt="Identi.ca" title="Identi.ca" /></a>');
      if(isset($kde_contributors[$data['user']]['opendesktop']) and !empty($kde_contributors[$data['user']]['opendesktop'])) echo('<a href="http://openDesktop.org/usermanager/search.php?username='.$kde_contributors[$data['user']]['opendesktop'].'"><img hspace="5" src="/images/pics/opendesktop.png" border="0" alt="openDesktop" title="openDesktop" /></a>');
      if(isset($kde_contributors[$data['user']]['facebook']) and !empty($kde_contributors[$data['user']]['facebook'])) echo('<a href="'.$kde_contributors[$data['user']]['facebook'].'"><img hspace="5" src="/images/pics/facebook.png" border="0" alt="Facebook" title="Facebook" /></a>');
      if(isset($kde_contributors[$data['user']]['blogurl']) and !empty($kde_contributors[$data['user']]['blogurl'])) echo('<a href="'.$kde_contributors[$data['user']]['blogurl'].'"><img hspace="5" src="/images/pics/blog.png" border="0" alt="Blog" title="Blog" /></a>');
      if(isset($kde_contributors[$data['user']]['kdesupportingmember']) and $kde_contributors[$data['user']]['kdesupportingmember']) echo('<a href="http://ev.kde.org"><img hspace="5" src="/images/pics/kde.png" border="0" alt="KDE Supporting Member" title="KDE Supporting Member" /></a>');
      echo('</td></tr>');

    }
    echo('</table></div>');
    DB::free_result($request);

  }




}


?>
