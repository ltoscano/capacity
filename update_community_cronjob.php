#!/usr/bin/php -q
<?php

require __DIR__ . '/vendor/autoload.php';
require_once('includes/www_config.php');

$feed = new SimplePie();
$feed->set_feed_url('https://dot.kde.org/rss.xml');
$feed->enable_cache(false);
$feed->init();

// remove old stuff
$deleteJob = $dbConnection->prepare('DELETE FROM news');
$deleteJob->execute();

// add stuff
$addJob = $dbConnection->prepare('INSERT INTO news (title, url, timestamp) values(:title, :permalink, :date)');
$items = $feed->get_items(0, 10);
foreach ($items as $item){
    $addJob->execute([
        'title' => addslashes($item->get_title()),
        'permalink' => addslashes($item->get_permalink()),
        'date' => addslashes(strtotime($item->get_date())),
    ]);
}
