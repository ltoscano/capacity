<?php
header("Content-type: text/css");
?>
/* Eric Meyer CSS Reset */
html, body, div, span, applet, object, iframe,h1, h2, h3, h4, h5, h6, p, blockquote, pre,a, abbr, acronym, address, big, cite, code,del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td {
	margin: 0;
	padding: 0;
	border: 0;
	outline: 0;
	font-size: 100%;
	vertical-align: baseline;
	background: transparent;
}
body { line-height: 1; }
ol, ul { list-style: none; }
blockquote, q {	quotes: none; }
blockquote:before, blockquote:after,q:before, q:after {	content: ''; content: none; }

/* remember to define focus styles! */
:focus { outline: 0; }

/* remember to highlight inserts somehow! */
ins { text-decoration: none; }
del { text-decoration: line-through; }

/* tables still need 'cellspacing="0"' in the markup */
table { border-collapse: collapse; border-spacing: 0; }

body { text-align: center; font-family:"Liberation Sans","Bitstream Vera Sans",sans-serif; background: #235E9A; }

.root {	background: #235E9A; }

.header { background: #235E9A url(images/header.mail.top.jpg) no-repeat top center; width: 100%; height: 244px; }

/* Top menu */
.menu_box div {
	color: #ffffff;
	display: table;
	text-align: center;
	width: auto;
	margin: 0 auto;
}

	.menu_box li {
		float: left;
		list-style-type: none;
		margin: 28px 8px;
		text-align: center;
	}

		.menu_box li a {
			color: #ffffff;
			height: 27px;
			text-decoration: none;
			margin: 0px 10px;
			line-height: 19px;
			display: block;
		}

		.menu_box li a:hover, .menu li a#current {
			background: transparent url(images/selector-center.png) no-repeat bottom center;
		}

			.menu_box li a span {
				margin-right: -10px;
				padding-right: 12px;
			}

			.menu_box li a:hover span, .menu li a#current span  {
				background: transparent url(images/select-right.png) no-repeat top right;
			}

				.menu_box li a span span {
					margin-left: -10px;
					margin-right: 0px;
					padding-left: 24px;
				}
				
				.menu_box li a:hover span span, .menu li a#current span span {
					background: transparent url(images/select-left.png) no-repeat top left;
				}

.teaser {
	background: transparent url(images/showcase.bg.png) no-repeat;
	width: 100%;
	max-width: 757px;
	height: 404px;
	margin: 0 auto;
}

.content, .footer {
	background: #ffffff url(images/header.mail.bottom.jpg) no-repeat top center;
	text-align: left;
	margin: 0px auto;
	max-width: 1200px;
}

	.content h2, .content a { color: #235E9A; }

.footer {
	background: #235E9A url(images/footer.jpg) no-repeat bottom center;
	min-height: 350px;
	text-align: left;
}

#main { width: 65%; float: left;

	#main p, #main h2, #main table, #main a { margin: 7px; }

#sidebar { float: left;	width: 29%; }

#module { min-height: 181px; min-width: 336px; margin-bottom: 20px; }

	#module h2, #module table, #module a { margin: 8px; padding-left: 4px; }

	#module h2 { padding-top: 8px; }

.content #module { background: transparent url(images/sidebar-box.png) no-repeat top left; }

.footer #module { background: transparent url(images/footer-box.png) no-repeat top left; }

a.cp-doNotDisplay { display: none; }
